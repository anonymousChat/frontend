FROM node:carbon AS build

WORKDIR /home/node
COPY . ./
RUN npm install

ARG REACT_APP_API_HOST
ENV REACT_APP_API_HOST ${REACT_APP_API_HOST}
ENV NODE_ENV production

RUN npm run build

##2

FROM nginx as release

WORKDIR /home/nginx

COPY nginx.conf /etc/nginx/conf.d/docker.conf
COPY --from=build /home/node/build www/

EXPOSE 3000

ENTRYPOINT ["nginx",  "-g", "daemon off;"]
