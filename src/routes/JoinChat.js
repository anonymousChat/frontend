import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { message as antdMessage } from 'antd'
import { getOrCreateUser } from '../services/user'
import JoinChatView from '../components/chat/JoinChatForm'

class JoinChat extends Component {
  handleStartChat = user => {
    getOrCreateUser(user)
      .then(userData => {
        this.props.onUserChange(userData)
      })
      .catch(error => {
        antdMessage.error(error.message)
      })
  }

  render() {
    return <JoinChatView onUsernameSubmit={this.handleStartChat} />
  }
}
JoinChat.propTypes = {
  onUserChange: PropTypes.func.isRequired
}

export default JoinChat
