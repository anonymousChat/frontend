import React from 'react'
import { shallow } from 'enzyme'

import JoinChat from './JoinChat'

const user = {
  username: 'Homer'
}

let props

jest.mock('../services/user', () => ({
  getOrCreateUser: jest.fn()
}))

const { getOrCreateUser } = require('../services/user')

afterEach(() => {
  jest.restoreAllMocks()
})

describe('JoinChat', () => {
  beforeEach(() => {
    props = {
      onUserChange: jest.fn()
    }
  })

  it('renders without crashing', () => {
    const wrapper = shallow(<JoinChat onUserChange={props.onUserChange} />)

    expect(wrapper.exists()).toBe(true)
    expect(wrapper).toMatchSnapshot()
  })

  describe('resolves', () => {
    beforeEach(() => {
      getOrCreateUser.mockResolvedValue({
        id: 12,
        username: 'Homer'
      })
    })
    it('calls and resolves getOrCreateUser endpoint when user name is submitted', async () => {
      const wrapper = shallow(<JoinChat onUserChange={props.onUserChange} />)

      wrapper
        .find('Form(JoinChatForm)')
        .props()
        .onUsernameSubmit(user)

      expect(getOrCreateUser).toBeCalledWith(user)
      await getOrCreateUser(user).then(result => {
        expect(result.id).toBe(12)
      })
    })
  })

  describe('rejects', () => {
    beforeEach(() => {
      getOrCreateUser.mockRejectedValue(new Error('Error occurred'))
    })
    it('reject getOrCreateUser endpoint when user name is submitted', async () => {
      const wrapper = shallow(<JoinChat onUserChange={props.onUserChange} />)

      wrapper
        .find('Form(JoinChatForm)')
        .props()
        .onUsernameSubmit(user)

      expect(getOrCreateUser).toBeCalledWith(user)
      return getOrCreateUser().catch(e =>
        expect(e.toString()).toEqual('Error: Error occurred')
      )
    })
  })
})
