import React from 'react'
import { shallow } from 'enzyme'

import JoinChat from './JoinChat'
import ChatWindow from './ChatWindow'

const user = {
  id: 12,
  username: 'Homer'
}

const message1 = {
  messageId: 10,
  message: 'I have a message',
  userName: 'Homer',
  messageDate: 1540724026959
}

const message2 = {
  messageId: 12,
  message: 'I have another message',
  userName: 'Homer',
  messageDate: 1540924026959
}

let props

jest.mock('../services/message', () => ({
  getMessages: jest.fn(),
  sendMessage: jest.fn()
}))

const { getMessages, sendMessage } = require('../services/message')

afterEach(() => {
  jest.restoreAllMocks()
})

describe('ChatWindow', () => {
  beforeEach(() => {
    props = {
      user: user
    }
  })

  describe('initialization', () => {
    beforeEach(() => {
      getMessages.mockResolvedValue([message1])
    })
    it('renders without crashing', () => {
      const wrapper = shallow(<ChatWindow user={props.user} />)

      expect(wrapper.exists()).toBe(true)
      expect(wrapper).toMatchSnapshot()
    })
    it('calls getMessages endpoint in componentDidMount cycle', async () => {
      shallow(<ChatWindow user={props.user} />)

      expect(getMessages).toHaveBeenCalledWith(undefined)
      await getMessages(undefined).then(result => {
        expect(result).toEqual([message1])
      })
    })
  })

  describe('hasMoreMessages', () => {
    it('does not render Load Previous Messages button when there is no more messages', () => {
      const wrapper = shallow(<ChatWindow user={props.user} />)

      wrapper.setState({ hasMoreMessages: false })

      expect(wrapper.find('.loadMoreButton').length).toBe(0)
    })

    it('renders Load Previous Messages button when there are more old messages', () => {
      const wrapper = shallow(<ChatWindow user={props.user} />)

      wrapper.setState({ hasMoreMessages: true })

      expect(wrapper.find('.noMessageWarn').length).toBe(0)
      expect(wrapper.find('.loadMoreButton').length).toBe(1)
    })

    it('loads more messages when clicking on load more button', async () => {
      getMessages.mockResolvedValue([message2])
      const wrapper = shallow(<ChatWindow user={props.user} />)

      wrapper.setState({ hasMoreMessages: true, messages: [message1] })

      wrapper
        .find('Button')
        .props()
        .onClick()

      expect(getMessages).toHaveBeenCalledWith(message1.messageDate)
      await getMessages(message1.messageDate).then(result => {
        expect(result).toEqual([message2])
      })
    })
  })

  describe('messageList component', () => {
    it('renders MessageList with messages non-empty array', () => {
      const wrapper = shallow(<ChatWindow user={props.user} />)

      wrapper.setState({ messages: [message1, message2] })

      expect(wrapper.find('.chatMessageList').length).toBe(1)
      expect(wrapper.find('.chatMessageList').props().messages).toEqual([
        message1,
        message2
      ])
    })

    it('renders MessageList with empty messages array', () => {
      const wrapper = shallow(<ChatWindow user={props.user} />)

      wrapper.setState({ messages: [] })

      expect(wrapper.find('.chatMessageList').length).toBe(1)
      expect(wrapper.find('.chatMessageList').props().messages).toEqual([])
    })
  })

  describe('messageForm component - success case', () => {
    beforeEach(() => {
      sendMessage.mockResolvedValue([message2])
    })
    it('sends new message successfully', async () => {
      const wrapper = shallow(<ChatWindow user={props.user} />)
      wrapper.setState({ hasMoreMessages: true, messages: [message1] })

      wrapper
        .find('Form(MessageForm)')
        .props()
        .onMessageSend(message2)

      expect(sendMessage).toHaveBeenCalled()
      await sendMessage(message2).then(result => {
        expect(result).toEqual([message2])
      })
    })
  })

  describe('messageForm component - failure case', () => {
    beforeEach(() => {
      sendMessage.mockRejectedValue(new Error('Error occurred'))
    })
    it('does not send message if api returns with failure', async () => {
      const wrapper = shallow(<ChatWindow user={props.user} />)
      wrapper.setState({ hasMoreMessages: true, messages: [message1] })

      wrapper
        .find('Form(MessageForm)')
        .props()
        .onMessageSend(message2)

      expect(sendMessage).toBeCalledWith(message2)
      return sendMessage().catch(e =>
        expect(e.toString()).toEqual('Error: Error occurred')
      )
    })
  })

  describe('webSocket', () => {
    it('binds SockJsClient', () => {
      const wrapper = shallow(<ChatWindow user={props.user} />)

      expect(wrapper.find('SockJsClient')).toBeDefined()
      const sockJsClientProps = wrapper.find('SockJsClient').props()
      expect(sockJsClientProps.url).toEqual(expect.stringContaining('/ws'))
      expect(sockJsClientProps.topics).toEqual(['/message'])
    })
  })

  describe('getMessagesWithOffset', () => {
    it('updates state.messages in reverse order when new messages arrive', async () => {
      getMessages.mockResolvedValue([message2])
      const wrapper = shallow(<ChatWindow user={props.user} />)
      await Promise.resolve() //wait for componentDidMount async tasks

      wrapper.setState({ hasMoreMessages: true, messages: [message1] })

      await wrapper.instance().getMessagesWithOffset()
      expect(wrapper.state().messages).toEqual([message2, message1])
    })

    it('does not update state.messages if there is no new message', async () => {
      getMessages.mockResolvedValue([])
      const wrapper = shallow(<ChatWindow user={props.user} />)
      await Promise.resolve() //wait for componentDidMount async tasks

      wrapper.setState({ hasMoreMessages: true, messages: [message1] })

      await wrapper.instance().getMessagesWithOffset()
      expect(wrapper.state().messages).toEqual([message1])
    })

    it('updates hasMoreMessages flag to false if there is no older messages', async () => {
      getMessages.mockResolvedValue([])
      const wrapper = shallow(<ChatWindow user={props.user} />)
      await Promise.resolve() //wait for componentDidMount async tasks

      wrapper.setState({ hasMoreMessages: true, messages: [message1] })

      await wrapper.instance().getMessagesWithOffset()
      expect(wrapper.state().hasMoreMessages).toBeFalsy()
    })
  })
})
