import React, { Component } from 'react'

import MessageList from '../components/message/MessageList'
import MessageForm from '../components/send/MessageForm'
import { sendMessage, getMessages } from '../services/message'
import SockJsClient from 'react-stomp'
import { Button, message as antdMessage } from 'antd'
import config from '../config'
import PropTypes from 'prop-types'
import styles from './ChatWindow.module.css'
import c from 'classnames'

class ChatWindow extends Component {
  constructor(props) {
    super(props)
    this.state = {
      messages: [],
      hasMoreMessages: true
    }
    this.WEB_SOCKET_URL = `${config.api.host}/ws`
    this.bottomElementRef = React.createRef()
  }

  componentDidMount() {
    this.getMessagesWithOffset().then(this.scrollToBottom)
  }

  onMessageReceive = (msg, topic) => {
    console.log(`A new message arrived via topic: ${topic}`)
    this.setState({ messages: [...this.state.messages, msg] })
    this.scrollToBottom()
  }

  handleMessageSending = message => {
    const data = {
      message: message.message,
      user_id: this.props.user.id,
      cdate: Date.now()
    }
    sendMessage(data)
      .then(() => {
        console.log('success')
      })
      .catch(error => {
        antdMessage.error(error.message)
      })
  }

  getMessagesWithOffset = async offset => {
    try {
      const newMessages = (await getMessages(offset)).reverse()
      if (newMessages.length > 0) {
        this.setState({ messages: [...newMessages, ...this.state.messages] })
      } else {
        this.setState({ hasMoreMessages: false })
        antdMessage.info('You reached the end. No more new messages.')
      }
    } catch (error) {
      antdMessage.error(error.message)
    }
  }

  getOldestMessageTimestamp = () => {
    const { messages } = this.state
    if (messages.length > 0) {
      return messages[0].messageDate
    }
    return null
  }

  scrollToBottom = () => {
    this.bottomElementRef.current.scrollIntoView({ behavior: 'smooth' })
  }

  loadMoreMessages = () => {
    this.getMessagesWithOffset(this.getOldestMessageTimestamp())
  }

  render() {
    const { messages, hasMoreMessages } = this.state
    const { user } = this.props
    return (
      <>
        {hasMoreMessages && (
          <Button
            className={c('loadMoreButton', styles.flexSteady)}
            onClick={this.loadMoreMessages}
          >
            Load Previous Messages
          </Button>
        )}

        <SockJsClient
          url={this.WEB_SOCKET_URL}
          topics={['/message']}
          onMessage={this.onMessageReceive}
        />
        <MessageList
          ref={this.bottomElementRef}
          messages={messages}
          currentUserName={user.username}
          className="chatMessageList"
        />
        <MessageForm onMessageSend={this.handleMessageSending} />
      </>
    )
  }
}

ChatWindow.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired
  }).isRequired
}

export default ChatWindow
