import React from 'react'
import { shallow, mount } from 'enzyme'

import MessageForm from './MessageForm'

const message = 'Hey hey, lets go to cinema'

let props

describe('MessageForm', () => {
  beforeEach(() => {
    props = {
      onMessageSend: jest.fn()
    }
  })

  it('renders without crashing', () => {
    const wrapper = shallow(<MessageForm onMessageSend={props.onMessageSend} />)

    expect(wrapper.exists()).toBe(true)
    expect(wrapper).toMatchSnapshot()
  })

  it('binds form elements correctly', () => {
    const wrapper = mount(<MessageForm onMessageSend={props.onMessageSend} />)

    expect(wrapper.find('Form')).toBeDefined()
    expect(wrapper.find('Input')).toBeDefined()
    expect(wrapper.find('Button')).toBeDefined()
    expect(wrapper.find('Button').text()).toEqual('Send')
  })

  it('fills message input with correct value', () => {
    const wrapper = mount(<MessageForm onMessageSend={props.onMessageSend} />)

    wrapper.find('input').simulate('change', {
      target: { value: message }
    })

    expect(wrapper.find('Input').props().value).toEqual(message)
  })

  it('submit the form when clicking on send message button, if message exist', () => {
    let prevented = false
    const wrapper = mount(<MessageForm onMessageSend={props.onMessageSend} />)

    wrapper.find('input').simulate('change', {
      target: { value: message }
    })

    wrapper.find('form').simulate('submit', {
      preventDefault: () => {
        prevented = true
      }
    })
    expect(prevented).toBe(true)
    expect(props.onMessageSend).toHaveBeenCalledWith({ message: message })
  })

  it('does not submit the form when message field is empty, shows a validation message', () => {
    const wrapper = mount(<MessageForm onMessageSend={props.onMessageSend} />)

    wrapper.find('form').simulate('submit', {})

    expect(props.onMessageSend).not.toHaveBeenCalled()
    expect(wrapper.find('.ant-form-explain').text()).toBe(
      'Please input your message!'
    )
  })
})
