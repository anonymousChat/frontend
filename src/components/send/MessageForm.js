import React from 'react'
import PropTypes from 'prop-types'
import { Button, Form, Input } from 'antd'

const FormItem = Form.Item

class MessageForm extends React.Component {
  handleSubmit = e => {
    const { validateFields, resetFields } = this.props.form
    e.preventDefault()
    validateFields((err, values) => {
      if (err) {
        return
      }
      this.props.onMessageSend(values)
      resetFields()
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem style={{ marginBottom: 0 }}>
          {getFieldDecorator('message', {
            rules: [{ required: true, message: 'Please input your message!' }]
          })(<Input placeholder="Type your message" />)}
        </FormItem>
        <FormItem style={{ textAlign: 'right' }}>
          <Button type="primary" htmlType="submit">
            Send
          </Button>
        </FormItem>
      </Form>
    )
  }
}

MessageForm.propTypes = {
  onMessageSend: PropTypes.func.isRequired
}

export default Form.create()(MessageForm)
