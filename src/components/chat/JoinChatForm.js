import React from 'react'
import { Form, Input, Button, Icon } from 'antd'
import PropTypes from 'prop-types'

const FormItem = Form.Item

class JoinChatForm extends React.Component {
  handleSubmit = e => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (err) {
        return
      }
      this.props.onUsernameSubmit(values)
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    return (
      <Form onSubmit={this.handleSubmit} className="chatForm">
        <FormItem>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please input your username!' }]
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Username"
            />
          )}
        </FormItem>
        <FormItem>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            Start Chat!
          </Button>
        </FormItem>
      </Form>
    )
  }
}

JoinChatForm.propTypes = {
  onUsernameSubmit: PropTypes.func.isRequired
}

export default Form.create()(JoinChatForm)
