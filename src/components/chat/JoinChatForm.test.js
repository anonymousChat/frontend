import React from 'react'
import { shallow, mount } from 'enzyme'

import JoinChatForm from './JoinChatForm'

const user = {
  username: 'Homer'
}
let props

describe('JoinChatForm', () => {
  beforeEach(() => {
    props = {
      onUsernameSubmit: jest.fn()
    }
  })

  it('renders without crashing', () => {
    const wrapper = shallow(
      <JoinChatForm onUsernameSubmit={props.onUsernameSubmit} />
    )

    expect(wrapper.exists()).toBe(true)
    expect(wrapper).toMatchSnapshot()
  })

  it('binds form elements correctly', () => {
    const wrapper = mount(
      <JoinChatForm onUsernameSubmit={props.onUsernameSubmit} />
    )

    expect(wrapper.find('Form')).toBeDefined()
    expect(wrapper.find('Input')).toBeDefined()
    expect(wrapper.find('Button')).toBeDefined()
    expect(wrapper.find('Button').text()).toEqual('Start Chat!')
  })

  it('fills username input with correct value', () => {
    const wrapper = mount(
      <JoinChatForm onUsernameSubmit={props.onUsernameSubmit} />
    )

    wrapper.find('input').simulate('change', {
      target: { value: user.username }
    })

    expect(wrapper.find('Input').props().value).toEqual(user.username)
  })

  it('submit the form when clicking on start chat button, if username exist', () => {
    let prevented = false
    const wrapper = mount(
      <JoinChatForm onUsernameSubmit={props.onUsernameSubmit} />
    )

    wrapper.find('input').simulate('change', {
      target: { value: user.username }
    })

    wrapper.find('form').simulate('submit', {
      preventDefault: () => {
        prevented = true
      }
    })
    expect(prevented).toBe(true)
    expect(props.onUsernameSubmit).toHaveBeenCalledWith(user)
  })

  it('does not submit the form when username field is empty, shows a validation message', () => {
    const wrapper = mount(
      <JoinChatForm onUsernameSubmit={props.onUsernameSubmit} />
    )

    wrapper.find('form').simulate('submit', {})

    expect(props.onUsernameSubmit).not.toHaveBeenCalled()
    expect(wrapper.find('.ant-form-explain').text()).toBe(
      'Please input your username!'
    )
  })
})
