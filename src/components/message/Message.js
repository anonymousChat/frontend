import React from 'react'
import PropTypes from 'prop-types'
import { Card, Avatar, Tag } from 'antd'
import styles from './Message.module.css'
import c from 'classnames'

const { Meta } = Card

function Message({ message, currentUserName }) {
  const { messageId, message: messageText, userName, messageDate } = message
  return (
    <div
      id={`message-${messageId}`}
      className={c(
        'message',
        styles.message,
        currentUserName === userName ? styles.myMessage : null
      )}
    >
      <div className={styles.messageBox}>
        <Tag>
          <div className="dateText">{getReadableDate(messageDate)}</div>
        </Tag>
        <Card className={styles.card}>
          <Meta
            avatar={<Avatar className={styles.avatar}>{userName[0]}</Avatar>}
            title={userName}
            description={messageText}
          />
        </Card>
      </div>
    </div>
  )
}

export const messageType = PropTypes.shape({
  messageId: PropTypes.number.isRequired,
  message: PropTypes.string.isRequired,
  userName: PropTypes.string.isRequired,
  messageDate: PropTypes.number.isRequired
})

Message.propTypes = {
  message: messageType,
  currentUserName: PropTypes.string.isRequired
}

function getReadableDate(date) {
  if (!date) {
    return ''
  }
  return new Intl.DateTimeFormat('en-GB', {
    year: 'numeric',
    month: 'short',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit'
  }).format(new Date(date))
}

export default Message
