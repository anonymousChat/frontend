import React from 'react'
import { shallow, mount } from 'enzyme'

import MessageList from './MessageList'

const messages = [
  {
    messageId: 12,
    message: 'How are you?',
    userName: 'Homer',
    messageDate: 1540724026959
  },
  {
    messageId: 13,
    message: 'Fine',
    userName: 'Marge',
    messageDate: 1540724029959
  }
]
let props

describe('MessageList', () => {
  beforeEach(() => {
    props = {
      messages: messages,
      currentUserName: 'Homer'
    }
  })

  it('renders without crashing', () => {
    const wrapper = shallow(
      <MessageList
        messages={props.messages}
        currentUserName={props.currentUserName}
      />
    )

    expect(wrapper.exists()).toBe(true)
    expect(wrapper).toMatchSnapshot()
  })

  it('binds form elements correctly', () => {
    const wrapper = shallow(
      <MessageList
        messages={props.messages}
        currentUserName={props.currentUserName}
      />
    )

    expect(wrapper.find('.messageList')).toBeDefined()
    expect(wrapper.find('Message').length).toEqual(2)
  })

  it('does not crash while rendering, if messages has an empty array', () => {
    const wrapper = shallow(
      <MessageList messages={[]} currentUserName={props.currentUserName} />
    )

    expect(wrapper.find('.messageList')).toBeDefined()
    expect(wrapper.find('Message').length).toEqual(0)
  })
})
