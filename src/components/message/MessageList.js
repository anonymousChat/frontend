import React from 'react'
import PropTypes from 'prop-types'
import Message, { messageType } from './Message'
import styles from './MessageList.module.css'

const MessageList = React.forwardRef(({ messages, currentUserName }, ref) => {
  return (
    <div className={styles.messageList}>
      {messages.map(m => (
        <Message
          key={m.messageId}
          message={m}
          currentUserName={currentUserName}
        />
      ))}
      <div style={{ float: 'left', clear: 'both' }} ref={ref} />
    </div>
  )
})

MessageList.propTypes = {
  messages: PropTypes.arrayOf(messageType).isRequired,
  currentUserName: PropTypes.string.isRequired
}

export default MessageList
