import React from 'react'
import { shallow, mount } from 'enzyme'

import Message from './Message'

const message = {
  messageId: 12,
  message: 'How are you?',
  userName: 'Homer',
  messageDate: 1540724026959
}
let props

describe('Message', () => {
  beforeEach(() => {
    props = {
      message: message,
      currentUserName: message.userName
    }
  })

  it('renders without crashing', () => {
    const wrapper = shallow(
      <Message
        message={props.message}
        currentUserName={props.currentUserName}
      />
    )

    expect(wrapper.exists()).toBe(true)
    expect(wrapper).toMatchSnapshot()
  })

  it('binds form elements correctly', () => {
    const wrapper = mount(
      <Message
        message={props.message}
        currentUserName={props.currentUserName}
      />
    )

    expect(wrapper.find(`#message-${message.messageId}`)).toBeDefined()
    expect(wrapper.find('.ant-card-meta-title').text()).toEqual(
      `${message.userName}`
    )
    expect(wrapper.find('.ant-card-meta-description').text()).toEqual(
      `${message.message}`
    )
    expect(wrapper.find('.dateText').text()).toEqual('Oct 28, 2018, 11:53 AM')
  })

  describe('myMessage css', () => {
    it('applies css for current user messages', () => {
      const wrapper = shallow(
        <Message
          message={props.message}
          currentUserName={props.currentUserName}
        />
      )
      expect(wrapper.find('.myMessage').length).toBe(1)
    })

    it('does not apply css for current user messages', () => {
      const wrapper = shallow(
        <Message message={props.message} currentUserName="Marge" />
      )
      expect(wrapper.find('.myMessage').length).toBe(0)
    })
  })
})
