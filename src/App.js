import React, { Component } from 'react'
import styles from './App.module.css'
import { Layout } from 'antd'
import ChatWindow from './routes/ChatWindow'
import JoinChat from './routes/JoinChat'
import c from 'classnames'

const { Header, Content } = Layout

class App extends Component {
  constructor() {
    super()
    this.state = {
      user: null
    }
  }

  setUser = user => {
    this.setState({ user })
  }

  render() {
    const { user } = this.state
    return (
      <Layout className={styles.layout}>
        <Header className={styles.header} />
        <Content className={styles.content}>
          <div className={c('chat', styles.chat)}>
            {user && <ChatWindow user={user} />}
            {!user && <JoinChat onUserChange={this.setUser} />}
          </div>
        </Content>
      </Layout>
    )
  }
}

export default App
