import React from 'react'
import { shallow, mount } from 'enzyme'

import App from './App'

const user = { id: 1, username: 'some name' }

describe('App', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<App />)

    expect(wrapper.exists()).toBe(true)
    expect(wrapper).toMatchSnapshot()
  })

  it('binds form elements correctly', () => {
    const wrapper = shallow(<App />)

    expect(wrapper.find('Layout')).toBeDefined()
    expect(wrapper.find('Content')).toBeDefined()
    expect(wrapper.find('.chat')).toBeDefined()
  })

  describe('When user loggedIn', () => {
    it('renders ChatWindow', () => {
      const wrapper = shallow(<App />)
      wrapper.setState({ user: user })

      expect(wrapper.find('ChatWindow')).toBeDefined()
      expect(wrapper.find('JoinChat').length).toBe(0)
    })
  })

  describe('When user is not loggedIn', () => {
    it('renders JoinChat', () => {
      const wrapper = shallow(<App />)
      wrapper.setState({ user: null })

      expect(wrapper.find('ChatWindow').length).toBe(0)
      expect(wrapper.find('JoinChat')).toBeDefined()
    })

    it('renders ChatWindow if user logs in', () => {
      const wrapper = shallow(<App />)

      expect(wrapper.find('ChatWindow').length).toBe(0)

      wrapper
        .find('JoinChat')
        .props()
        .onUserChange(user)

      expect(wrapper.find('ChatWindow')).toBeDefined()
    })
  })
})
