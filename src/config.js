const config = {
  api: {
    host: process.env.REACT_APP_API_HOST
  }
}

export default config
