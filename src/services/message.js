import api from './api'

function sendMessage(data) {
  return api.post('/messages', data).then(({ data }) => data)
}

function getMessages(offset = Date.now()) {
  const limit = 3
  return api
    .get(`/messages?limit=${limit}&offset=${offset}`)
    .then(({ data }) => data)
}

export { sendMessage, getMessages }
