import axios from 'axios'
import config from '../config'

const ax = axios.create({
  baseURL: config.api.host,
  timeout: 5000
})

export default ax
