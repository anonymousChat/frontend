import api from './api'

function getOrCreateUser(data) {
  return api.post('/users', data).then(({ data }) => data)
}

export { getOrCreateUser }
