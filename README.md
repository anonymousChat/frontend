# Frontend
> Chat Frontend

## Setting up dev environment
Run `docker-compose up` and you're ready to go.
If you need to rebuild your image, use `docker-compose build`

`docker-compose down && docker-compose build && docker-compose up`

## Dev ports

```
 http://localhost:3000
 
```

## Build Setup 
```
npm start
    Starts the development server.

npm run build
    Bundles the app into static files for production.

npm test
    Starts the test runner.

npm run eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!
```

## Tech Stack

1. React
    - I choose React, because it is light-weight, low file size and it is a go to library for all kinds of frontend applications nowadays. 
2. Jest
3. Enzyme
4. Antd (as a React UI Framework)
4. Docker

## Next Steps
1. Instead of 'Load Previous Messages' button, provide reverse infinite scrolling for 
loading previous messages
2. Setup production environment

## Video Recording of Application
 
Click to watch [ChatVideo](https://www.dropbox.com/s/c97y30sgp6o5kpj/chatVideo.mov?dl=0)
